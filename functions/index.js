const functions = require('firebase-functions');

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

const userRouter = require("./routes/userApp.js");
const matchRouter = require("./routes/matchApp.js");

app.use('/users',userRouter);
app.use('/matches',matchRouter);

exports.api = functions.https.onRequest(app);