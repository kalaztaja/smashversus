const admin = require('firebase-admin');
admin.initializeApp({
    credential: admin.credential.applicationDefault()
});
const db = admin.firestore();

///USERS

async function getUserByNick(nick){
    var endUser = [];
    const userRef = await db.collection('users').where('nickname','==',nick).get().then(snapshot => {
        snapshot.docs.forEach(returnUser => {
            endUser.push({
                id: returnUser.id,
                data: returnUser.data()
            });
        })
    });
    return endUser
}

async function getUserById(id){
    const reference = await db.collection('users').doc(id).get();
    return reference.data();
}

async function getAllUsers(){
    const allUsers = await db.collection('users').get();
    returnUsers = [];
    allUsers.forEach((doc) => {
        returnUsers.push({
            id: doc.id,
            data: doc.data()
        });
    });
    return returnUsers;
}

async function createUser(text){
    const parsed = {
        "fullname": text.fullname,
        "nickname": text.nickname,
        "elo": 1000
    }
    const reference = await db.collection('users').add(parsed);
    const user = await reference.get();
    return user.data();
}


///MATCHES

async function getAllMatches(){
    const allMatches = await db.collection('matches').get();
    returnMatches = []
    allMatches.forEach((doc) => {
        returnMatches.push({
            id: doc.id,
            data: doc.data()
        });
    });
    return returnMatches;
};

async function postMatch(json){
    //TODO
    const user1 = await getUserByNick(json.player1nick);
    const user2 = await getUserByNick(json.player2nick);
    return user1;
};



module.exports = {
    getUserByNick, getUserById,getAllUsers,createUser,getAllMatches,postMatch
}