const matchRouter = require('express').Router();
const db = require("../db/db.js");

matchRouter.post('/', async(req,res) => {
    try{
        const values = req.body;
        const parsedValues = {
            "player1nick": values.player1,
            "player2nick": values.player2,
            "winner": values.winner,
            "score": values.score
        }
        //TODO
        const returnValue = await db.postMatch(parsedValues);
        res.send(returnValue);
    }
    catch(error){
        res.status(500).send(error);
    }
});
matchRouter.get('/', async(req,res) => {
    const returnValue = await db.getAllMatches();
    res.json(returnValue);
});

module.exports = matchRouter;