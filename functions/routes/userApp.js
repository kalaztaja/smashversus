const userRouter = require('express').Router();

const db = require('../db/db.js');

userRouter.get('/', async (req,res) => {
    const users = await db.getAllUsers();
    res.json(users);
});
userRouter.post('/', async (request,response) =>{
    try{
        const text = request.body;
        

        const returnValue = await db.createUser(text);
        response.send(returnValue);
    }
    catch(error){
        response.status(500).send(error);
    }
})
userRouter.get('/:id', async(req,res) => {
    try{
        const id = req.params.id;
        if(!id) throw new error('Id is required!');

        const requestedUser = await db.getUserById(id);
        res.json(requestedUser);
    }
    catch(error){
        res.status(500).send(error);
    }
});
userRouter.get('/nick/:nickname', async(req,res) => {
    try {
        const nick = req.params.nickname;
        if(!nick) throw new error('Nickname is required!');

        const userRef = await db.getUserByNick(nick);
        res.send(userRef);
    }
    catch(error){
        res.status(500).send(error);
    }
});

module.exports = userRouter;