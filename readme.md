## This is a rest-backend for the smashVersus system.

This should be deployed and you can reach the interface at:
https://us-central1-pinigsbase.cloudfunctions.net/api/

To develope this you need to follow these steps:

1. Clone this repository
2. Go to functions folder and `npm install --save`
3. While you at it run `npm install -g firebase-tools`
4. After that login to firebase tools using `firebase login`
5. To get access to the project **send your gmail account** to martikkalajohannes@gmail.com
6. After that you are all **done**!

If you have any questions please reach out. If you want something implemented open a issue about it!